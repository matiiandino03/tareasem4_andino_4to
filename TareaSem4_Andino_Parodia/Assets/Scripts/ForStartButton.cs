using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class ForStartButton : ButtonTT
{
    public Flowchart flowchart;
    public GameObject box;
    public Transform spawnPos;
    public GameObject thisExample;

    bool pressed = false;
    public override void ButtonPressed()
    {
        if(!pressed)
        {
            StartCoroutine("SpawnBoxes");
            pressed = true;
        }
        
    }

    IEnumerator SpawnBoxes()
    {
        yield return new WaitForSeconds(0.8f);
        Instantiate(box, spawnPos.transform.position,Quaternion.identity);
        yield return new WaitForSeconds(0.8f);
        Instantiate(box, spawnPos.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(0.8f);
        Instantiate(box, spawnPos.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(0.8f);
        Instantiate(box, spawnPos.transform.position, Quaternion.identity);
        yield return new WaitForSeconds(0.8f);
        Instantiate(box, spawnPos.transform.position, Quaternion.identity);
        thisExample.SetActive(false);
        flowchart.ExecuteBlock("ForeachExplain");
    }
}
