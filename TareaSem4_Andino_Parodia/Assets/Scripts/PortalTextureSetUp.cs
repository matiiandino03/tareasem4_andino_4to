using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTextureSetUp : MonoBehaviour
{
    public Camera cameraB;

    public Material cameraMatB;
    void Start()
    {
        if(cameraB.targetTexture != null)
        {
            cameraB.targetTexture.Release();
        }
        cameraB.targetTexture = new RenderTexture(Screen.width,Screen.width,24);
        cameraMatB.mainTexture = cameraB.targetTexture;
    }

}
