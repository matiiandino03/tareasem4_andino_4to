using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerThirdPart : MonoBehaviour
{
    public ThirdPart thirdPart;

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Box")
        {
            thirdPart.BoxInZone();
        }
    }
}
