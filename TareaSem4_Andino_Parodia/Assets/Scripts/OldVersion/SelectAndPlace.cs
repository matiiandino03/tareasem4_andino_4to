using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Fungus;

public class SelectAndPlace : MonoBehaviour
{
    public Flowchart flowchart;
    public Toggle toggle;
    public GameObject jugadorPrefab;
    public GameObject playerSpawned;
    public bool holding = false;
    public LayerMask groundMask;

    public Camera cameraMain;
    bool moveCamera = false;

    public List<Rigidbody> walls = new List<Rigidbody>();

    public Transform spawnLvl2;
    bool unaSolaVez = false;

    public GameObject lvl2Ground;
    public GameObject firstPart;
    SecondPart secondPart;

    private void Awake()
    {
        Screen.SetResolution(1920,1080, true);
    }
    private void Start()
    {
        GetComponent<Button>().enabled = false;
        GetComponent<Image>().enabled = false;
        //StartCoroutine("TextHelp");
        secondPart = FindObjectOfType<SecondPart>();
    }
    void Update()
    {
        RaycastHit hit;
        Ray ray = cameraMain.ScreenPointToRay(Input.mousePosition);
        if (holding)
        {
            if (Physics.Raycast(ray, out hit, float.MaxValue, groundMask))
            {
                playerSpawned.transform.position = new Vector3(hit.point.x,hit.point.y + 1,hit.point.z);

                 if (Input.GetMouseButtonDown(0))
                 {
                    holding = false;
                    moveCamera = true;
                    //GameManager.instance.unityHelpPanel.SetActive(false);
                 }
            }               

        }
        if(moveCamera)
        {
            cameraMain.transform.LookAt(playerSpawned.transform.position);
            cameraMain.transform.Translate(cameraMain.transform.forward*17*Time.deltaTime);
            if(Vector3.Distance(cameraMain.transform.position,playerSpawned.transform.position)<1)
            {
                moveCamera = false;
                cameraMain.gameObject.SetActive(false);
                playerSpawned.GetComponentInChildren<Camera>().enabled = true;
                playerSpawned.GetComponentInChildren<AudioListener>().enabled = true;
                playerSpawned.GetComponentInChildren<CameraController>().enabled = true;
                foreach(Rigidbody rb in walls)
                {
                    rb.isKinematic = false;
                }
                StartCoroutine("fallWalls");
            }
        }

        if (playerSpawned != null && playerSpawned.transform.position.y < -3 && !holding && !unaSolaVez)
        {
            flowchart.ExecuteBlock("New Block2");
            unaSolaVez = true;
        }
    }

    public void PickPlayer()
    {
        holding = true;
        playerSpawned = Instantiate(jugadorPrefab, new Vector3(-50,-50,-50), Quaternion.identity);
        GetComponent<Button>().enabled = false;
        GetComponent<Image>().enabled = false;
    }
    IEnumerator fallWalls()
    {
        yield return new WaitForSeconds(2.3f);
        foreach (Rigidbody rb in walls)
        {
            rb.isKinematic = true;
        }
    }

    public void ActivatePlayerButton()
    {      
        GetComponent<Button>().enabled = true;
        GetComponent<Image>().enabled = true;
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene("SecondLesson");
    }

}

