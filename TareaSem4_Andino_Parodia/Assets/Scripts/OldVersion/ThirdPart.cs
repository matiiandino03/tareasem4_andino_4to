using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class ThirdPart : MonoBehaviour
{
    public GameObject btBuild;
    public GameObject Player;
    public List<RectTransform> spawnPositions = new List<RectTransform>();
    public List<Image> errors = new List<Image>();
    bool onlyOnce = false;
    private void Update()
    {
        if(Player != null && Vector3.Distance(Player.transform.position,btBuild.transform.position)<2.5f && Input.GetKey(KeyCode.E) && !onlyOnce)
        {
            InvokeRepeating("Error", 0, 0.2f);
            InvokeRepeating("Error", 0, 0.2f);
            InvokeRepeating("Error", 0, 0.2f);
            InvokeRepeating("Error", 0, 0.2f);
            onlyOnce = true;
            StartCoroutine("EndGame");
        }
    }
    public void StartThirdP()
    {
        StartCoroutine("StartThirdPart");
        Player = GameObject.FindGameObjectWithTag("Player");
    }
    public void  BoxInZone()
    {
        StartCoroutine("NotWorking");
    }

    public void StartLastPart()
    {
        StartCoroutine("LastPart");
    }

    IEnumerator StartThirdPart()
    {
        //GameManager.instance.txtHelp.text = "Bueno, para evitar mas bugs esta vez haremos algo simple";
        yield return new WaitForSeconds(3);
        //GameManager.instance.txtHelp.text = "Lo unico que debes hacer es empujar la caja roja hacia la zona amarilla, esto deberia abrir la puerta marron";
    }

    IEnumerator NotWorking()
    {
        //GameManager.instance.txtHelp.text = "No puedo creerlo! Esto tampoco funciona!";
        yield return new WaitForSeconds(3);
        //GameManager.instance.txtHelp.text = "Por alguna razon no esta detectando el trigger. Se me ocurre solo una forma de solucionarlo y poder seguir con la explicacion.";
        yield return new WaitForSeconds(3);
        //GameManager.instance.txtHelp.text = "Recuerdas que antes te dije que no utilizes lo de traspasar paredes, bueno... me parece que la unica solucion es que traspases la pared marron";
    }

    IEnumerator LastPart()
    {
        //GameManager.instance.txtHelp.text = "Bueno, lo ultimo que tienes que hacer una vez terminado el juego es hacer la Build.";
        yield return new WaitForSeconds(3);
        //GameManager.instance.txtHelp.text = "Esto te dara el ejecutable de tu juego para que puedas probarlo fuera de Unity";
        yield return new WaitForSeconds(3);
        //GameManager.instance.txtHelp.text = "No deberia ser dificil, lo unico que debes hacer es acercarte al boton que tienes en frente y presionar E";

    }

    void Error()
    {
        int posRand = Random.Range(0, spawnPositions.Count);
        int errorRand = Random.Range(0, 1);
        Instantiate(errors[errorRand], spawnPositions[posRand]);
    }

    IEnumerator EndGame()
    {
        yield return new WaitForSeconds(1);
        //GameManager.instance.txtHelp.text = "�Nooo! Se rompio todo el juego, no puedo creerlo!";
        yield return new WaitForSeconds(3);
        //GameManager.instance.txtHelp.text = "No se como solucionar esto, te devolvere al menu";
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("Menu2");
        //cambiar logo a la foto de godot
    }
}
