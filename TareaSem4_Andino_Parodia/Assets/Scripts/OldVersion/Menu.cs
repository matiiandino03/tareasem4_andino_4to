using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    public GameObject MainMenu;
    public Text txtmessage;
    private void Start()
    {
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
    }
    public void StartGame()
    {
        SceneManager.LoadScene("Game");
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    public void Options()
    {
        txtmessage.text = "No hay nada aqui";
        StartCoroutine("Nothing");
    }
    public void Multiplayer()
    {
        txtmessage.text = "De verdad pensaste que habia un modo multijugador?";
        StartCoroutine("Nothing");
    }

    IEnumerator Nothing()
    {
        MainMenu.SetActive(false);
        txtmessage.gameObject.SetActive(true);
        yield return new WaitForSeconds(3);
        txtmessage.gameObject.SetActive(false);
        MainMenu.SetActive(true);
    }
}
