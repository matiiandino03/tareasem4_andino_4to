using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerLastPart : MonoBehaviour
{
    public ThirdPart thirdPart;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            thirdPart.StartLastPart();
        }
    }
}
