using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondPart : MonoBehaviour
{
    public GameObject missinTextureObjects;
    public GameObject thirdPart;
    public GameObject thirdPartSpawn;
    GameObject Jugador;
    public bool start = false;

    public Animator closeEyes;
    private void Update()
    {
        if(start)
        {
            StartCoroutine("Begin");
            start = false;
            Jugador = GameObject.FindGameObjectWithTag("Player");
        }
    }

    IEnumerator Begin()
    {
        yield return new WaitForSeconds(2);
        //GameManager.instance.txtHelp.text = "Esta zona esta un poco vacia. �Te parece si la decoramos un poco?";
        yield return new WaitForSeconds(3);
        //GameManager.instance.txtHelp.text = "Voy a buscar un pack en la AssetStore para decorar esta zona. Cierra los ojos que quiero ver como te sorprendes";
        closeEyes.SetTrigger("Close");
        yield return new WaitForSeconds(4);
        //GameManager.instance.txtHelp.text = "Ahora si, abre los ojos en 3.... 2..... 1!!!";
        yield return new WaitForSeconds(4);
        closeEyes.SetTrigger("Open");
        missinTextureObjects.SetActive(true);
        ///GameManager.instance.txtHelp.text = "La verdad nose que esta pasando hoy, nada funciona! Me estoy avergonzando a mi mismo";
        yield return new WaitForSeconds(3);
        //GameManager.instance.txtHelp.text = "Bueno sigamos, te llevare a la ultima zona donde te ense�are a hacer puzzle utilizando los metodos OnTirggerEnter y OnTirggerExit";
        yield return new WaitForSeconds(4);
        thirdPart.SetActive(true);
        Jugador.transform.position = thirdPartSpawn.transform.position;
        thirdPart.GetComponent<ThirdPart>().StartThirdP();
        this.gameObject.SetActive(false);
    }
}
