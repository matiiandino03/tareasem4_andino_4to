using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class ForeachExampleBt : ButtonTT
{
    public Flowchart flowchart;
    public GameObject[] boxes;
    public GameObject example;
    bool isFirst = false;
    public override void ButtonPressed()
    {
        if(!isFirst)
        {
            StartCoroutine("ChangeColor");
            isFirst = true;
        }
       
    }

    IEnumerator ChangeColor()
    {
        yield return new WaitForSeconds(0.8f);
        boxes[0].GetComponent<Renderer>().material.color = Color.blue;
        yield return new WaitForSeconds(0.8f);
        boxes[1].GetComponent<Renderer>().material.color = Color.blue;
        yield return new WaitForSeconds(0.8f);
        boxes[2].GetComponent<Renderer>().material.color = Color.blue;
        yield return new WaitForSeconds(0.8f);
        boxes[3].GetComponent<Renderer>().material.color = Color.blue;
        yield return new WaitForSeconds(0.8f);
        boxes[4].GetComponent<Renderer>().material.color = Color.blue;
        yield return new WaitForSeconds(0.8f);
        boxes[5].GetComponent<Renderer>().material.color = Color.blue;
        yield return new WaitForSeconds(0.8f);
        example.SetActive(false);
        flowchart.ExecuteBlock("WhileExplain");
    }

}
