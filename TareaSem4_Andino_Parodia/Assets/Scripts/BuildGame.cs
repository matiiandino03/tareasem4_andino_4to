using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class BuildGame : MonoBehaviour
{
    public GameObject buildButton;
    public GameObject buildButtonMat;
    public Material errorMat;
    public Animator buildbtAnim;
    public Transform player;
    public bool hasPressed = false;
    public static BuildGame instance;
    public Flowchart flowchart;

    public GameObject UnityCrash;

    public PlayerController pc;
    public CameraController cc;
    public AudioSource audioSource;
    public AudioClip warningSound;
    public GameObject warningLights;

    public GameObject black;

    private void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && Vector3.Distance(buildButton.transform.position,player.position) < 2)
        {
            buildbtAnim.SetBool("Error", true);
            pc.rapidezDesplazamientoSprint = 4;
            audioSource.PlayOneShot(warningSound);
            warningLights.SetActive(true);
            buildButtonMat.GetComponent<Renderer>().material = errorMat;
            hasPressed = true;
            flowchart.ExecuteBlock("New Block1");
        }
    }

    public void CrashGame()
    {
        cc.enabled = false;
        pc.enabled = false;
        UnityCrash.SetActive(true);
        audioSource.volume = 0;
        StartCoroutine("CloseGame");
    }

    IEnumerator CloseGame()
    {
        GameManager.instance.reachedLvlWhile = false;
        GameManager.instance.ReachedWhileLvl();
        UnityCrash.SetActive(true);
        yield return new WaitForSeconds(1);
        black.SetActive(true);
        yield return new WaitForSeconds(2);
        Application.Quit();
    }


}
