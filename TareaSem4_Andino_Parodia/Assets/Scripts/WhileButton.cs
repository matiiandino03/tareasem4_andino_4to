using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhileButton : ButtonTT
{
    public CameraController cc;
    public PlayerController pc;
    public GameObject UnityCrash;
    public override void ButtonPressed()
    {
        GameManager.instance.reachedLvlWhile = true;
        GameManager.instance.ReachedWhileLvl();
        StartCoroutine("CloseGame");
    }

    IEnumerator CloseGame()
    {
        cc.enabled = false;
        pc.enabled = false;
        yield return new WaitForSeconds(1);
        UnityCrash.SetActive(true);
        yield return new WaitForSeconds(2);
        Application.Quit();
    }
}
