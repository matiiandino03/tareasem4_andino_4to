using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField]
    public bool reachedLvlWhile = false;
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        if (instance != null && instance != this)
        {
            Destroy(this);
        }
        else
        {
            instance = this;
        }
    }
    private void Start()
    {
        reachedLvlWhile = LoadData();
        if(reachedLvlWhile)
        {
            SceneManager.LoadScene("ThirdLessonFail");
        }        
    }

    public void ReachedWhileLvl()
    {      
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/playerData.while";
        FileStream stream = new FileStream(path, FileMode.Create);

        formatter.Serialize(stream, reachedLvlWhile);
        stream.Close();
    }
    public bool LoadData()
    {
        string path = Application.persistentDataPath + "/playerData.while";
        if(File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);

            bool reachedLvl = (bool)formatter.Deserialize(stream);
            stream.Close();
            return reachedLvl;
        }
        else
        {
            return false;
        }
    }
}
