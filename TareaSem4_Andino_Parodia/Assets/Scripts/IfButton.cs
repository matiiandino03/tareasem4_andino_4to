using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class IfButton : ButtonTT
{
    public Flowchart flowchart;
    public override void ButtonPressed()
    {
        flowchart.ExecuteBlock("ForExplain");
    }
}
