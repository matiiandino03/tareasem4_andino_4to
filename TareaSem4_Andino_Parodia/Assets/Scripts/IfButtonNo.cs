using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fungus;

public class IfButtonNo : ButtonTT
{
    public Flowchart flowchart;
    public override void ButtonPressed()
    {
        flowchart.ExecuteBlock("IfExplainAgain");
    }

    public void StartFor()
    {
        flowchart.ExecuteBlock("ForExplain");
    }
}
