using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineRendererMovement : MonoBehaviour
{
    [SerializeField]
    PlayerController pc;
    [SerializeField]
    CameraController cc;

    public Material notUsing;
    public Material isUsing;

    LineRenderer lr;

    bool onPuzzle = false;

    public int lvlNumber;

    public SecondLesson sl;
    void Start()
    {
        lr = GetComponent<LineRenderer>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && !onPuzzle && Vector3.Distance(transform.position,pc.gameObject.transform.position)< 2)
        {
            pc.enabled = false;
            cc.enabled = false;
            lr.material = isUsing;
            onPuzzle = true;
        }
        else if (Input.GetKeyDown(KeyCode.E) && onPuzzle)
        {
            onPuzzle = false;
            pc.enabled = true;
            cc.enabled = true;
            lr.material = notUsing;
            lr.positionCount = 2;
        }
        if(onPuzzle && Input.GetKeyDown(KeyCode.RightArrow))
        {
            if(lr.GetPosition(lr.positionCount - 1).x > -0.39f)
            {
                bool normal = false;
                if (lr.GetPosition(lr.positionCount - 2) == new Vector3(lr.GetPosition(lr.positionCount - 1).x - 0.2f, lr.GetPosition(lr.positionCount - 1).y, lr.GetPosition(lr.positionCount - 1).z))
                {
                    lr.positionCount--;
                    normal = true;
                }
                if(!normal)
                {
                    for (int i = 0; i < lr.positionCount - 1; i++)
                    {
                        if (lr.GetPosition(i) == new Vector3(lr.GetPosition(lr.positionCount - 1).x - 0.2f, lr.GetPosition(lr.positionCount - 1).y, lr.GetPosition(lr.positionCount - 1).z))
                        {
                            StartCoroutine("Error");
                            return;
                        }
                    }
                }              
                if (!normal)
                {
                    lr.positionCount++;
                    lr.SetPosition(lr.positionCount - 1, new Vector3(lr.GetPosition(lr.positionCount - 2).x - 0.2f, lr.GetPosition(lr.positionCount - 2).y, lr.GetPosition(lr.positionCount - 2).z));                
              
                }
            }
            else
            {
                StartCoroutine("Error");
            }
        }
        if (onPuzzle && Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (lr.GetPosition(lr.positionCount - 1).x < 0.39f)
            {
                bool normal = false;
                if (lr.GetPosition(lr.positionCount - 2) == new Vector3(lr.GetPosition(lr.positionCount - 1).x + 0.2f, lr.GetPosition(lr.positionCount - 1).y, lr.GetPosition(lr.positionCount - 1).z))
                {
                    lr.positionCount--;
                    normal = true;
                }
                if(!normal)
                {
                    for (int i = 0; i < lr.positionCount - 1; i++)
                    {
                        if (lr.GetPosition(i) == new Vector3(lr.GetPosition(lr.positionCount - 1).x + 0.2f, lr.GetPosition(lr.positionCount - 1).y, lr.GetPosition(lr.positionCount - 1).z))
                        {
                            StartCoroutine("Error");
                            return;
                        }
                    }
                }               
                if (!normal)
                {
                    lr.positionCount++;
                lr.SetPosition(lr.positionCount - 1, new Vector3(lr.GetPosition(lr.positionCount - 2).x + 0.2f, lr.GetPosition(lr.positionCount - 2).y, lr.GetPosition(lr.positionCount - 2).z));
                }
            }
            else
            {
                StartCoroutine("Error");
            }
        }
        if (onPuzzle && Input.GetKeyDown(KeyCode.UpArrow))
        {
            if (lr.GetPosition(lr.positionCount - 1).y > -0.39f)
            {
                bool normal = false;
                if (lr.GetPosition(lr.positionCount - 2) == new Vector3(lr.GetPosition(lr.positionCount - 1).x, lr.GetPosition(lr.positionCount - 1).y - 0.2f, lr.GetPosition(lr.positionCount - 1).z))
                {
                    lr.positionCount--;
                    normal = true;
                }
                if(!normal)
                {
                    for (int i = 0; i < lr.positionCount - 1; i++)
                    {
                        if (lr.GetPosition(i) == new Vector3(lr.GetPosition(lr.positionCount - 1).x, lr.GetPosition(lr.positionCount - 1).y - 0.2f, lr.GetPosition(lr.positionCount - 1).z))
                        {
                            StartCoroutine("Error");
                            return;
                        }
                    }
                }          
                if (!normal)
                {
                    lr.positionCount++;
                lr.SetPosition(lr.positionCount - 1, new Vector3(lr.GetPosition(lr.positionCount - 2).x, lr.GetPosition(lr.positionCount - 2).y - 0.2f, lr.GetPosition(lr.positionCount - 2).z));
                }
            }
            else
            {
                StartCoroutine("Error");
            }
        }
        if (onPuzzle && Input.GetKeyDown(KeyCode.DownArrow))
        {
            if (lr.GetPosition(lr.positionCount - 1).y < 0.39f)
            {
                bool normal = false;
                if (lr.GetPosition(lr.positionCount - 2) == new Vector3(lr.GetPosition(lr.positionCount - 1).x, lr.GetPosition(lr.positionCount - 1).y + 0.2f, lr.GetPosition(lr.positionCount - 1).z))
                {
                    lr.positionCount--;
                    normal = true;
                }
                if(!normal)
                {
                    for (int i = 0; i < lr.positionCount - 1; i++)
                    {
                        if (lr.GetPosition(i) == new Vector3(lr.GetPosition(lr.positionCount - 1).x, lr.GetPosition(lr.positionCount - 1).y + 0.2f, lr.GetPosition(lr.positionCount - 1).z))
                        {
                            StartCoroutine("Error");
                            return;
                        }
                    }
                }                
                if(!normal)
                {
                    lr.positionCount++;
                    lr.SetPosition(lr.positionCount - 1, new Vector3(lr.GetPosition(lr.positionCount - 2).x, lr.GetPosition(lr.positionCount - 2).y + 0.2f, lr.GetPosition(lr.positionCount - 2).z));
                }
            }
            else
            {
                StartCoroutine("Error");
            }
                
        }

        if(lr.GetPosition(lr.positionCount - 1) == new Vector3(-0.4f, -0.4f,-0.53f))
        {
            if(lvlNumber == 1)
            {
                CheckWinLvl1();
            }
            if (lvlNumber == 2)
            {
                CheckWinLvl2();
            }
        }
    }

    void CheckWinLvl1()
    {
        if(lr.GetPosition(3) == new Vector3(0,0.4f,-0.53f))
        {
            if (lr.GetPosition(5) == new Vector3(0, 0f, -0.53f))
            {
                if (lr.GetPosition(8) == new Vector3(-0.4f, -0.2f, -0.53f))
                {
                    if (lr.GetPosition(12) == new Vector3(0.4f, -0.2f, -0.53f))
                    {
                        if (lr.GetPosition(13) == new Vector3(0.4f, -0.4f, -0.53f))
                        {
                            sl.lvl2.enabled = true;
                            onPuzzle = false;
                            pc.enabled = true;
                            cc.enabled = true;
                            lr.material = notUsing;
                            lr.material.color = Color.green;
                            sl.lvl1.enabled = false;
                            return;
                        }
                    }
                }
            }
        }
        StartCoroutine("Error");
        lr.positionCount = 2;
    }
    void CheckWinLvl2()
    {
        if (lr.GetPosition(2) == new Vector3(0.4f, 0.2f, -0.53f))
        {
            if (lr.GetPosition(4) == new Vector3(0, 0.2f, -0.53f))
            {
                if (lr.GetPosition(5) == new Vector3(0f, 0f, -0.53f))
                {
                    if (lr.GetPosition(9) == new Vector3(-0.4f, 0.4f, -0.53f))
                    {
                        if (lr.GetPosition(12) == new Vector3(-0.4f, -0.2f, -0.53f))
                        {
                            if (lr.GetPosition(17) == new Vector3(0.4f, 0f, -0.53f))
                            {
                                if (lr.GetPosition(19) == new Vector3(0.4f, -0.4f, -0.53f))
                                {
                                    onPuzzle = false;
                                    pc.enabled = true;
                                    cc.enabled = true;
                                    lr.material = notUsing;
                                    lr.material.color = Color.green;
                                    sl.LevelCompleted();
                                    sl.lvl2.enabled = false;                                    
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }


            StartCoroutine("Error");
        lr.positionCount = 2;
    }

    IEnumerator Error()
    {
        lr.material = notUsing;
        lr.material.color = Color.red;
        yield return new WaitForSeconds(0.2f);
        lr.material.color = Color.white;
        lr.material = isUsing;
    }

}
