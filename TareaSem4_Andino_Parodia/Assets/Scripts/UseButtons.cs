using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UseButtons : MonoBehaviour
{
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit hit;
            if(Physics.Raycast(Camera.main.transform.position,Camera.main.transform.forward,out hit,3))
            {
                if(hit.transform.gameObject.GetComponent<ButtonTT>())
                {
                    hit.transform.gameObject.GetComponent<ButtonTT>().ButtonPressed();
                }
            }
        }
    }
}
