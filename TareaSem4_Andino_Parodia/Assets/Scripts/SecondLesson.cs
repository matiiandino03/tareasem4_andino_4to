using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Fungus;

public class SecondLesson : MonoBehaviour
{
    public LineRendererMovement lvl1;
    public LineRendererMovement lvl2;

    public Animator BlackScreen;
    public Flowchart flowchart;

    public void StartTransition()
    {
        BlackScreen.SetBool("Transition", true);
    }

    public void LevelCompleted()
    {
        flowchart.ExecuteBlock("New Block1");
    }
    public void ChangeLevel()
    {
        SceneManager.LoadScene("ThirdLesson");
    }
}
