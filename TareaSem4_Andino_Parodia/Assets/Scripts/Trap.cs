using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trap : MonoBehaviour
{
    public Transform spawnPoint;
    public GameObject error;
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Player" && BuildGame.instance.hasPressed)
        {
            Instantiate(error, spawnPoint.position,Quaternion.identity);
        }
    }
}
