using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Resolution_Handler : MonoBehaviour
{
    public GameObject VideoIntro;
    public GameObject TemporalBlack;
    void Awake()
    {
        Screen.SetResolution(1020, 601, false);
        StartCoroutine("StopIntroVideo");
    }

    public void ChangeScene()
    {
        SceneManager.LoadScene("LoadingProject");
    }
    IEnumerator StopIntroVideo()
    {
        yield return new WaitForSeconds(0.2f);
        TemporalBlack.SetActive(false);
        yield return new WaitForSeconds(1f);
        VideoIntro.SetActive(false);
    }
}
