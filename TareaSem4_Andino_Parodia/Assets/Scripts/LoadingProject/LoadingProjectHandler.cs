using System.Collections;
using System.Collections.Generic;
using UnityEngine; 
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingProjectHandler : MonoBehaviour
{
    public Image LoadingBar;
    void Start()
    {
        Screen.SetResolution(614, 571, false);
        StartCoroutine("FirstLoad");
    }

    IEnumerator FirstLoad()
    {
        LoadingBar.fillAmount = 0.1f;
        yield return new WaitForSeconds(3);
        LoadingBar.fillAmount = 0.15f;
        yield return new WaitForSeconds(4);
        LoadingBar.fillAmount = 0.18f;
        yield return new WaitForSeconds(5);
        LoadingBar.fillAmount = 0.29f;
        yield return new WaitForSeconds(3);
        LoadingBar.fillAmount = 0.45f;
    }
    public void LoadTwentyPercent()
    {
        LoadingBar.fillAmount += 0.20f;
    }
    public void ChangeScene()
    {
        SceneManager.LoadScene("FirstLesson");
    }

}
